from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SingupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


def account_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def account_signup(request):
    if request.method == "POST":
        form = SingupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

        if password == password_confirmation:
            user = User.objects.create_user(username, password=password)
            login(request, user)

            return redirect("list_projects")

        else:
            form.add_error("password", "the passwords do not match")
    else:
        form = SingupForm()

    context = {"form": form}

    return render(request, "registration/signup.html", context)
